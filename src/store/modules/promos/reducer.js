const promosReducer = (state = "", action) => {
  switch (action.type) {
    case "@promos/SET_PROMOS":
      return action.promos;

    default:
      return state;
  }
};

export default promosReducer;
