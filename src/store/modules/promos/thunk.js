import { setPromos } from "./actions";
import axios from "axios";

export const setPromosThunk = () => (dispatch, getState) => {
  axios.get("https://free-2-share-api.herokuapp.com/promos").then((res) => {
    dispatch(setPromos(res.data));
  });
};
