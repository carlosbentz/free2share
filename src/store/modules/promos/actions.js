export const setPromos = (promos) => ({
  type: "@promos/SET_PROMOS",
  promos,
});
