import addList from "./actions";

const addListThunk = (listFavorites) => (dispatch, getState) => {
  const { listOfFavorites } = getState();
  const newListFavorites = [...listFavorites];
  dispatch(addList(newListFavorites));
};
export default addListThunk;
