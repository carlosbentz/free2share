import {ADD_LIST} from './actionType'

const listOfFavoritesReducer = (state = [], action) => {
    switch(action.type){
      case ADD_LIST:
        const {listFavorites} = action
        return listFavorites
      default:
        return state
    }

}
export default listOfFavoritesReducer