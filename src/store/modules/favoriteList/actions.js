import { ADD_LIST } from "./actionType";

const addList = (listFavorites = []) => ({
  type: ADD_LIST,
  listFavorites,
});

export default addList;
