import React from "react";
import "./style.css";
import logo from "../../assets/logo/logo2.png";

import { Link, useLocation } from "react-router-dom";

const Header = () => {
  const location = useLocation();
  return (
    <header className="header">
      <div className="logo">
        <img src={logo} alt="logo" />
        <p>
          Free<span className="standOut">2</span>Share
        </p>
      </div>

      <div className="nav">
        <Link className={location.pathname === "/" ? "highlight" : ""} to="/">
          Home
        </Link>
        <Link
          className={location.pathname === "/login" ? "highlight" : ""}
          to="/login"
        >
          Login
        </Link>
        <Link
          className={location.pathname === "/register" ? "highlight" : ""}
          to="/register"
        >
          Cadastre-se
        </Link>
      </div>
    </header>
  );
};

export default Header;
