import "./style.css";

import Button from "../button";
import { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useSelector } from "react-redux";

export default function PromotionCard({ card, isFavorite }) {
  const history = useHistory();
  const location = useLocation();
  const [active, setActive] = useState(isFavorite);
  const token = useSelector((state) => state.token);
  // const [sucessMenssager,setSucessMenssager] = useState(false)

  const handleSelect = (card) => {
    PostData(card);
    setActive(!active);
  };

  const PostData = async (body) => {
    if (location.pathname !== "/") {
      const decoded = jwt_decode(token);
      body.isFavorite = !body.isFavorite;
      body.userId = decoded.sub;
      if (body.isFavorite) {
        try {
          const response = await axios.post(
            "https://free-2-share-api.herokuapp.com/favorites",
            body,
            { headers: { Authorization: `Bearer ${token}` } }
          );
          console.log(response);
          // setSucessMenssager(true)
        } catch (err) {
          console.log(err);
        }
      } else {
        try {
          body.isFavorite = !body.isFavorite;
          const response = await axios.delete(
            `https://free-2-share-api.herokuapp.com/favorites/${body.id}`,
            { headers: { Authorization: `Bearer ${token}` } }
          );
          // setSucessMenssager(true)
        } catch (err) {
          console.log(err);
        }
      }
    } else {
      console.log(body.preco);
    }
  };
  return (
    <>
      {" "}
      {card && (
        <div className="promotion-card">
          <img src={card.img} alt="game-name" className="game-image" />
          <div className="game-content">
            <div className="game-title-and-likes">
              <div
                className="title"
                onClick={() => history.push("/promos/" + card.id)}
              >
                <h1 className="title">{card.titulo}</h1>
                <p className="developer">by {card.plataforma}</p>
              </div>
              <div className="likes">
                <i
                  onClick={() => handleSelect(card)}
                  className={
                    "fas fa-thumbs-up like-button" + (active ? " favorite" : "")
                  }
                ></i>
              </div>
            </div>
            <div className="game-info">
              <div className="promotion-info">
                <h1 className="price">R$ - {card.preco}</h1>
                <p className="expiration-date">
                  Até dia:{" "}
                  {card.ExpirateDate ? card.ExpirateDate : "Indefinido"}
                </p>
              </div>
              <Button>
                Site
                <a
                  className="game-link"
                  href={card.link}
                  target="_blank"
                  rel="noreferrer"
                ></a>
              </Button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
