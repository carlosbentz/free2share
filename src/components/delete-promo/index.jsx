import "./style.css";
import axios from "axios";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";

export default function DeletPromo() {
  const token = useSelector((state) => state.token);
  const user = useSelector((state) => state.user);
  const promos = useSelector((state) => state.promos);
  const [userPromos, setUserPromos] = useState([]);

  useEffect(() => {
    setUserPromos(promos.filter((el) => parseInt(el.userId) === user.id));
  }, [promos]);

  const deletePromo = (promoIndex) => {
    axios
      .delete(
        "https://free-2-share-api.herokuapp.com/promos/" +
          userPromos[promoIndex].id,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
      .then((res) => {
        const newPromos = userPromos.filter((el) => {
          return el.id !== userPromos[promoIndex].id;
        });
        setUserPromos([...newPromos]);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="delete-promo">
      <h1 className="delete-promo">Promoções</h1>
      <div className="promos">
        {userPromos.map((el, index) => {
          return (
            <div className="promo" key={index}>
              <span className="promo-title">{el.titulo}</span>
              <a onClick={() => deletePromo(index)} className="delete-promo">
                Excluir
              </a>
            </div>
          );
        })}
      </div>
    </div>
  );
}
