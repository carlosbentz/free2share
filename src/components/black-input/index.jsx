import { makeStyles } from "@material-ui/core/styles";
import CssTextField from "./style";
import "./style.css";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function BlackInput({
  name,
  label,
  error,
  helperText,
  disabled = false,
  defaultValue = false,
  type,
  inputRef,
}) {
  const classes = useStyles();

  return (
    <CssTextField
      className={classes.margin}
      variant="outlined"
      id="custom-css-outlined-input"
      name={name}
      label={label}
      error={error}
      helperText={helperText}
      type={type}
      disabled={disabled && disabled}
      inputRef={inputRef}
    />
  );
}
