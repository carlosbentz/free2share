import "./style.css";
import NavButton from "../nav-button";

import { setTokenThunk } from "../../store/modules/token/thunk";
import logo from "../../assets/logo/logo2.png";

import Cookies from "js-cookie";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";

export default function NavBar() {
  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  const logout = () => {
    Cookies.get("token") && Cookies.remove("token");
    dispatch(setTokenThunk(""));
    history.push("/");
  };

  return (
    <div className="navbar">
      <div className="nav-top">
        <img src={logo} className="logo"></img>
        <span className="free2share">Free2Share</span>
      </div>
      <div className="navigation">
        <NavButton
          icon={<i className="fas fa-home"></i>}
          text="Home"
          transparent={location.pathname === "/home" ? false : true}
          path="/home"
        />
        <NavButton
          icon={<i className="fas fa-thumbs-up"></i>}
          text="Favoritos"
          transparent={location.pathname === "/favorites" ? false : true}
          path="/favorites"
        />
        <NavButton
          icon={<i className="fas fa-plus-circle"></i>}
          text="Ad. Promoção"
          transparent={location.pathname === "/add-promotion" ? false : true}
          path="/add-promotion"
        />
        <NavButton
          icon={<i className="fas fa-user-circle"></i>}
          text="Perfil"
          transparent={location.pathname === "/profile" ? false : true}
          path="/profile"
        />
      </div>
      <div className="nav-bottom">
        <div className="user">
          <img src={user && user.photo} className="logo"></img>
          <span className="username">{user && user.name.split(" ")[0]}</span>
        </div>
        <span className="logout">
          <i className="fas fa-sign-out-alt" onClick={logout}></i>
        </span>
      </div>
    </div>
  );
}
