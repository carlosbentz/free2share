import "./style.css";
import Input from "../input";
import Buttom from "../button";
import { updateUserThunk } from "../../store/modules/user/thunk";

import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

export default function ChangePassword() {
  // GLOBAL STATES
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const token = useSelector((state) => state.token);

  // LOCAL STATES
  const [isDisabled, setIsDisabled] = useState(true);

  // YUP VALIDATION
  const scheme = yup.object().shape({
    password: yup
      .string()
      .min(6, "Mínimo de 6 Dígitos")
      .matches(/^(?=.*?[#?!@$%^&*-])/, "Pelo menos um caracter especial.")
      .required("Campo obrigatório"),
    passwordConfirm: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas Diferentes")
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(scheme),
  });

  // FORM HANDLER
  const handleForm = (data) => {
    setIsDisabled(true);
    const newUser = {
      ...data,
      name: user.name,
      email: user.email,
      nickname: user.nickname,
      platform: user.platform,
      bio: user.bio,
      photo: user.photo,
    };
    dispatch(updateUserThunk(token, newUser));
  };

  return (
    <div className="password-change">
      <h1 className="password-change">Senha</h1>
      <form
        noValidate
        onSubmit={handleSubmit(handleForm)}
        className="password-change"
      >
        <h2 className="input-title">Nova Senha</h2>
        <Input
          name="password"
          error={errors.password}
          helperText={errors.password?.message}
          defaultValue=""
          disabled={isDisabled}
          type="password"
          inputRef={register}
        />
        <h2 className="input-title">Confirme nova senha</h2>
        <Input
          name="passwordConfirm"
          error={errors.passwordConfirm}
          helperText={errors.passwordConfirm?.message}
          defaultValue=""
          disabled={isDisabled}
          type="password"
          inputRef={register}
        />
        <div className="profile-buttons">
          <div
            className="button secondary"
            onClick={() => setIsDisabled(true)}
            secondary={true}
          >
            Cancelar
          </div>
          {isDisabled ? (
            <div
              className="button"
              onClick={() => setIsDisabled(false)}
              secondary={true}
            >
              Alterar
            </div>
          ) : (
            <Buttom>Salvar</Buttom>
          )}
        </div>
      </form>
    </div>
  );
}
