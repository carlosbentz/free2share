import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import NavBar from "../navbar";
import { Provider } from "react-redux";
import store from "../../store";
import { toHaveStyle } from "@testing-library/jest-dom";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/home",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe("When everything is ok", () => {
  test("Should change the button color", () => {
    render(
      <Provider store={store}>
        <NavBar></NavBar>
      </Provider>
    );
    userEvent.click(screen.getByText("Perfil"));
    expect(screen.getByText("Perfil")).toHaveStyle({
      backgroundColor: "rgb(144, 52, 216);",
    });
  });
});
