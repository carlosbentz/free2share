import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Footer from "../footer";
import { toHaveStyle } from "@testing-library/jest-dom";

describe("When everything is ok", () => {
  test("When the name is clicked shold open a new tab", () => {
    render(<Footer />);
    userEvent.hover(screen.getByText("Pedro H. Germano"));
    expect(screen.getByText("Pedro H. Germano")).toHaveStyle({
      color: "rgb(144, 52, 216);",
    });
  });
});
