import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { toBeChecked } from "@testing-library/jest-dom";

import LoginForm from "../login-form";
import { Provider } from "react-redux";
import store from "../../store";

describe("When everything is ok", () => {
  test("When label clicked style changes", () => {
    render(
      <Provider store={store}>
        <LoginForm />
      </Provider>
    );

    userEvent.click(screen.getByText("Mantenha-me conectado"));
    const changeSpan = screen.getByRole("checkbox");
    expect(changeSpan).toBeChecked();
  });
});
