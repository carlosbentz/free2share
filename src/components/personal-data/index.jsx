import "./style.css";
import Input from "../input";
import Buttom from "../button";
import { updateUserThunk } from "../../store/modules/user/thunk";

import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

export default function PersonalData() {
  // GLOBAL STATES
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const token = useSelector((state) => state.token);

  // LOCAL STATES
  const [isDisabled, setIsDisabled] = useState(true);

  // YUP VALIDATION
  const scheme = yup.object().shape({
    email: yup
      .string()
      .required("Campo Necessário")
      .email("Formato de email Inválido"),
    nickname: yup.string().required("Campo Necessário"),
    name: yup
      .string()
      .min(6, "Mínimo de 6   Dígitos")
      .matches(
        /^[a-zA-Z]{4,}(?: [a-zA-Z]+){1,2}$/,
        "Deve ter pelo menos nome e sobrenome, e não pode conter caracteres especiais."
      )
      .required("Campo obrigatório"),
    plataform: yup.string(),
    photo: yup
      .string()
      .matches(
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi,
        "Url Invalida"
      ),
    bio: yup.string(),
  });

  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(scheme),
  });

  // FORM HANDLER
  const handleForm = (data) => {
    setIsDisabled(true);
    const newUser = {
      ...data,
      password: user.password,
      passwordConfirm: user.passwordConfirm,
    };
    dispatch(updateUserThunk(token, newUser));
  };

  return (
    <div className="personal-data">
      <h1 className="personal-data">Dados Pessoais</h1>
      <form
        noValidate
        onSubmit={handleSubmit(handleForm)}
        className="personal-data"
      >
        <h2 className="input-title">Nome Completo</h2>
        <Input
          name="name"
          error={errors.name}
          helperText={errors.name?.message}
          defaultValue={user && user.name}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <h2 className="input-title">Email</h2>
        <Input
          name="email"
          error={errors.email}
          helperText={errors.email?.message}
          defaultValue={user && user.email}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <h2 className="input-title">Nickname</h2>
        <Input
          name="nickname"
          error={errors.nickname}
          helperText={errors.nickname?.message}
          defaultValue={user && user.nickname}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <h2 className="input-title">Plataforma</h2>
        <Input
          name="plataform"
          error={errors.plataform}
          helperText={errors.plataform?.message}
          defaultValue={user && user.platform}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <h2 className="input-title">Link para Foto</h2>
        <Input
          name="photo"
          error={errors.photo}
          helperText={errors.photo?.message}
          defaultValue={user && user.photo}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <h2 className="input-title">Bio</h2>
        <Input
          name="bio"
          error={errors.bio}
          helperText={errors.bio?.message}
          defaultValue={user && user.bio}
          disabled={isDisabled}
          type="text"
          inputRef={register}
        />
        <div className="profile-buttons">
          <div className="button secondary" onClick={() => setIsDisabled(true)}>
            Cancelar
          </div>
          {isDisabled ? (
            <div className="button" onClick={() => setIsDisabled(false)}>
              Alterar
            </div>
          ) : (
            <Buttom>Salvar</Buttom>
          )}
        </div>
      </form>
    </div>
  );
}
