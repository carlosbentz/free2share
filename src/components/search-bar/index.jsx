const SearchBar = ({ search, setSearch, promos }) => {
  return (
    <>
      <input
        type="text"
        placeholder="Faça sua busca!"
        onChange={(evt) => {
          setSearch(evt.target.value);
        }}
        value={search}
      />
    </>
  );
};

export default SearchBar;
