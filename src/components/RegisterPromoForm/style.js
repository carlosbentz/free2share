import styled from "styled-components";

export const Container = styled.div`
  width: calc(100vw - 325px);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-left: 325px;
  margin-top: 40px;
  box-sizing: border-box;
  padding: 16px;
  div {
    color: white;
    font-size: 30px;
    font-weight: bold;
    text-align: left;
    width: 100%;
  }

  h1 {
    color: #faf1ff;
    font-size: 30px;
    font-weight: bold;
    text-align: center;
    margin-bottom: 16px;
  }

  form {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
    box-sizing: border-box;
    width: 100%;
    max-width: 600px;

    background-color: #39345f;
    border-radius: 20px;
    padding: 30px;
  }
  fieldset {
    border: 2px solid #faf1ff;
    border-radius: 10px;
  }

  .MuiFormLabel-root.Mui-focused {
    color: #9034d8;
  }

  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline {
    border-color: #9034d8;
  }

  .MuiFormControl-root.MuiTextField-root {
    margin-bottom: 24px;
  }

  input {
    height: 18px;
    width: 100%;
    padding-top: 27px;
    border-radius: 10px;
    background-color: #0000;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
  }
  input:focus {
    border-color: #9034d8;
  }
  label {
    color: #faf1ff;
  }
  input::placeholder {
    font-size: 1.2rem;
    color: #faf1ff;
  }

  button {
    max-width: 320px;
    width: 100%;
    height: 56px;
    margin: 0 auto;
    border-radius: 8px;
    font-style: normal;
    font-weight: bold;
    font-size: 1.3rem;
    letter-spacing: 2px;
    font-size: 16px;
    cursor: pointer;
    line-height: 150%;
    color: white;
    background: #9034d8;
  }

  @media screen and (max-width: 770px) {
    margin-left: 242px;
    width: calc(100% - 242px);
  }
  @media screen and (max-width: 580px) {
    margin-left: 75px;
    width: calc(100% - 75px);
  }
`;
