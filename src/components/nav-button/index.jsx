import "./style.css";

import { useHistory } from "react-router-dom";

export default function NavButton({ icon, text, transparent, path }) {
  const history = useHistory();
  return (
    <button
      className={transparent === true ? "transparent nav-button" : "nav-button"}
      onClick={() => history.push(path)}
    >
      <span className="nav-button-icon">{icon}</span>
      <span className="nav-button-label">{text}</span>
    </button>
  );
}
