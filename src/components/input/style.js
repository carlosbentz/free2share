import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const CssTextField = withStyles({
  root: {
    "& .MuiFormLabel-root": {
      color: "#FAF1FF",
    },
    "& label.Mui-focused": {
      color: "#9034D8",
    },
    "& .MuiOutlinedInput-root": {
      color: "#FAF1FF",
      "& fieldset": {
        borderColor: "#FAF1FF",
      },
      "&:hover fieldset": {
        borderColor: "#FAF1FF",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#9034D8",
      },
    },

    // Fixing error colors
    "& label.Mui-focused.Mui-error": {
      color: "red",
    },
    "& .MuiOutlinedInput-root.Mui-error": {
      "& fieldset": {
        borderColor: "red",
      },
      "&:hover fieldset": {
        borderColor: "red",
      },
      "&.Mui-focused fieldset": {
        borderColor: "red",
      },
    },

    // Fixing disabled colors
    "& .MuiInputBase-input.Mui-disabled": {
      backgroundColor: "rgb(250, 241, 255, 0.2)",
    },
    "& .MuiFormLabel-root.Mui-disabled": {
      color: "#C1BAC5",
    },
    "& .MuiOutlinedInput-root.Mui-disabled": {
      color: "#C1BAC5",
      "& fieldset": {
        borderColor: "rgb(250, 241, 255, 0.2)",
      },
      "&:hover fieldset": {
        borderColor: "rgb(250, 241, 255, 0.2)",
      },
      "&.Mui-focused fieldset": {
        borderColor: "rgb(250, 241, 255, 0.2)",
      },
    },
  },
})(TextField);

export default CssTextField;
