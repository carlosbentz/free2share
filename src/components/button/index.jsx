import "./style.css";

export default function Button({ children, type, secondary, onClick }) {
  return (
    <button
      type={type && type}
      className={"button " + (secondary ? "secondary" : "")}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
