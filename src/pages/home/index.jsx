import PromotionCard from "../../components/promotion-card";
import NavBar from "../../components/navbar";
import SearchBar from "../../components/search-bar";

import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { setPromosThunk } from "../../store/modules/promos/thunk";

import "./style.css";

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setPromosThunk());
  }, []);

  const promos = useSelector((state) => state.promos);

  const [search, setSearch] = useState("");

  return (
    <div className="home-page">
      <NavBar />
      <div className="search-bar">
        <SearchBar search={search} setSearch={setSearch} promos={promos} />
      </div>
      <div className="cards">
        {promos &&
          promos
            .filter((game) => {
              if (search.length > 3) {
                return game.titulo.toLowerCase() === search.toLowerCase();
              } else {
                return promos;
              }
            })

            .map((promo, index) => {
              return (
                <PromotionCard card={promo} isFvorite={false} key={index} />
              );
            })}
      </div>
    </div>
  );
};

export default Home;
