import { makeStyles } from "@material-ui/core/styles";
import backgroungRegister from "../../assets/illustrations/backgroungRegister.svg";

const useStyles = makeStyles((theme) => ({
  margin: {
    marginBottom: "16px",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    paddingBottom: "20px",
    width: "25%",
    marginLeft: "4vw",
    "@media (max-width: 900px)": {
      margin: 0,
      width: "75%",
    },
    "@media (max-height: 900px )": {
      marginTop: "72px",
    },
  },
  input: {
    backgroundColor: "#9021d4",
    color: "white",
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "24px",
    letterSpacing: "0em",
    textAlign: "center",
    width: "100%",
    margin: theme.spacing(1),
    borderRadius: "0.4rem",
  },
  registerTitle: {
    color: "#9021d4",
    fontSize: "40px",
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "60px",
    letterSpacing: "0em",
    textAlign: "center",
    marginBottom: "5vh",
  },
  root: {
    backgroundImage: `url(${backgroungRegister})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    overflow: "hidden",

    fontFamily: "Rubik",
    display: "flex",
    alignItems: "center",
    paddingTop: "104px",
    "@media (max-width: 900px)": {
      backgroundImage: "none",
      backgroundColor: "#faf1ff",
      flexDirection: "column",
      overflow: "scroll",
    },

    "@media (max-height: 900px)": {
      overflow: "scroll",
      backgroundPosition: "top",
    },
  },

  registerText: {
    color: "#9021d4",
    textDecoration: "none",
    cursor: "pointer",
  },
  snackBar: {},
}));

export default useStyles;
