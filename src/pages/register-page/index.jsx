import "./style.css";
import Header from "../../components/header";
import Button from "../../components/button";
import useStyles from "./styles";

import CssTextField from "./styledTextField";
import { Box, Typography, Grid } from "@material-ui/core/";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { useState } from "react";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function RegisterPage() {
  const classes = useStyles();
  let user = {};
  const history = useHistory();
  const [open, setOpen] = useState(false);

  const schema = yup.object().shape({
    email: yup.string().email("Email Inválido").required("Campo obrigatório"),
    nickname: yup.string(),
    password: yup
      .string()
      .min(6, "Mínimo de 6 Dígitos")
      .matches(/^(?=.*?[#?!@$%^&*-])/, "Pelo menos um caracter especial.")
      .required("Campo obrigatório"),
    passwordConfirm: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas Diferentes")
      .required("Campo obrigatório"),
    name: yup
      .string()
      .min(6, "Mínimo de 6   Dígitos")
      .matches(
        /^[a-zA-Z]{4,}(?: [a-zA-Z]+){1,2}$/,
        "Deve ter pelo menos nome e sobrenome, e não pode conter caracteres especiais."
      )
      .required("Campo obrigatório"),
    platform: yup.string(),
    photo: yup.string(),
    bio: yup.string(),
  });

  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    user = { ...data };

    axios
      .post("https://free-2-share-api.herokuapp.com/users", user)
      .then((res) => history.push("/login"))
      .catch((err) => handleClick());
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <Header />
      <form
        noValidate
        className={classes.form}
        onSubmit={handleSubmit(handleForm)}
      >
        <Typography
          component="h3"
          variant="h3"
          className={classes.registerTitle}
        >
          Cadastro
        </Typography>
        <CssTextField
          className={classes.margin}
          label="Nome Completo"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.name}
          helperText={errors.name?.message}
          placeholder="Pedro Henrique"
          name="name"
          inputRef={register}
          required
        />
        <CssTextField
          className={classes.margin}
          label="E-Mail"
          variant="outlined"
          id="custom-css-outlined-input"
          placeholder="usuario@mail.com"
          error={!!errors.email}
          helperText={errors.email?.message}
          name="email"
          inputRef={register}
          required
        ></CssTextField>
        <CssTextField
          className={classes.margin}
          label="Senha"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.password}
          helperText={errors.password?.message}
          type="password"
          name="password"
          placeholder="*********"
          inputRef={register}
          required
        />
        <CssTextField
          className={classes.margin}
          label="Confirmar senha"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.passwordConfirm}
          helperText={errors.passwordConfirm?.message}
          type="password"
          name="passwordConfirm"
          placeholder="*********"
          inputRef={register}
          required
        ></CssTextField>
        <CssTextField
          className={classes.margin}
          label="Plataforma"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.platform}
          helperText={errors.platform?.message}
          placeholder="Steam"
          name="platform"
          inputRef={register}
        />
        <CssTextField
          className={classes.margin}
          label="Nickname"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.nickname}
          helperText={errors.nickname?.message}
          placeholder="20tematar"
          name="nickname"
          inputRef={register}
        ></CssTextField>
        <CssTextField
          className={classes.margin}
          label="Foto (Link)"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.photo}
          helperText={errors.photo?.message}
          placeholder="Insira o link de sua foto."
          name="photo"
          inputRef={register}
        ></CssTextField>
        <CssTextField
          className={classes.margin}
          label="Biografia"
          variant="outlined"
          id="custom-css-outlined-input"
          error={!!errors.bio}
          helperText={errors.bio?.message}
          multiline
          rows={2}
          name="bio"
          inputRef={register}
        />

        <Button type="submit">Cadastrar</Button>
        <Grid container justify="center">
          <Grid item>
            <span>
              Já possui uma conta?
              <a href="/login" className={classes.registerText}>
                Login
              </a>
            </span>
          </Grid>
        </Grid>
      </form>
      <Box className={classes.snackBar}>
        <Snackbar open={open} autoHideDuration={12000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="error">
            Credênciais invalidas.
          </Alert>
        </Snackbar>
      </Box>
    </Box>
  );
}
