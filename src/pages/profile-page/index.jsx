import "./style.css";
import Navbar from "../../components/navbar";
import PersonalData from "../../components/personal-data";
import ChangePassword from "../../components/change-password";
import DeletePromo from "../../components/delete-promo";

export default function ProfilePage() {
  return (
    <div className="profile-page">
      <Navbar />
      <PersonalData />
      <ChangePassword />
      <DeletePromo />
    </div>
  );
}
