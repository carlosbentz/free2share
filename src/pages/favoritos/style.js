import styled from "styled-components";

export const CardContainer = styled.div`

.Favoritepage {
  display: flex;
  flex-flow: nowrap column;
  margin-left: 28vw;
  font-family: "Rubik", sans-serif;
}
.cards {
  width: 72vw;
  height: 100vh;
  display: flex;
  
  flex-flow: wrap row;
  justify-content: space-around;
  align-items: center;
}

@media screen and (max-width: 768px) {
  .Favoritepage {
    margin-left: 32vw;
  }
}

@media screen and (max-width: 580px) {
  .Favoritepage {
    margin-left: 25vw;
  }

}

`