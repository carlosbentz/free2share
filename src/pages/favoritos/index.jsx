import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import {CardContainer} from './style'
import addListThunk from "../../store/modules/favoriteList/thunk";
import NavBar from "../../components/navbar";
import PromotionCard from "../../components/promotion-card";

const Favoritos = () => {
  const favoriteList = useSelector((state) => state.listFavorites);
  const token = useSelector((state) => state.token);
  const [error, setError] = useState();
  const dispatch = useDispatch();

  const getData = async () => {
    try {
      const response = await axios.get(
        "https://free-2-share-api.herokuapp.com/favorites",
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      );
      dispatch(addListThunk(response.data));
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    getData();
    favoriteList.length!==0 && getData() 
  }, [favoriteList]);

  return (
    <CardContainer>
    <div class ="Favoritepage">
      <div className="cards">
        <NavBar />
        {favoriteList.map((current, index) => {
          return <PromotionCard isFavorite={true} card={current} key={index} />;
        })}
      </div>
    </div>
    </CardContainer>
  );
};

export default Favoritos;
