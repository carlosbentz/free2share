import "./style.css";
import IllustrationLogin from "../../assets/illustrations/login-illustration";
import LoginForm from "../../components/login-form";
import Header from "../../components/header";

export default function LoginPage() {
  return (
    <div className="login-page">
      <Header />
      <div className="login-left">
        <h1 className="login-page-title">Bem vindo</h1>
        {IllustrationLogin}
      </div>{" "}
      <div className="login-right">
        <LoginForm />
      </div>
    </div>
  );
}
