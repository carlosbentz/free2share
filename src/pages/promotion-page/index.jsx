import "./style.css";
import Button from "../../components/button";
import { useSelector } from "react-redux";
import useStyles from "./styles";
import { Box, Typography, Grid, TextField, Link } from "@material-ui/core/";
import ThumbUpAltRoundedIcon from "@material-ui/icons/ThumbUpAltRounded";
import { useForm } from "react-hook-form";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
export default function PromotionPage() {
  const [testComments, setTestComments] = useState([
    {
      name: "Pedro",
      comment: "Muito obrigado por postar ! Estava querendo muito esse jogo.",
    },
    {
      name: "Paulo",
      comment: "Esse jogo é muito bom !",
    },
    {
      name: "João",
      comment:
        "Valeu cara, mais um jogo que eu não vou jogar, mas que irei adquirir ! ",
    },
    {
      name: "Ana",
      comment: "De graça até injeção na testa.",
    },
    {
      name: "Clóvis",
      comment: "A parte mais divertida desse game, foi clicar em desinstalar.",
    },
  ]);
  const classes = useStyles();
  let [promo, setPromo] = useState([]);
  const history = useHistory();
  const params = useParams();
  const user = useSelector((state) => state.user);
  const schema = yup.object().shape({
    comment: yup
      .string()
      .min(4, "Mínimo de 4 Dígitos")
      .max(140, "Máximo de 140 caracteres"),
  });
  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema),
  });
  const handleForm = (data) => {
    const testName = { name: user.name };
    data = { ...data, ...testName };
    setTestComments([...testComments, data]);
  };
  useEffect(() => {
    axios
      .get(`https://free-2-share-api.herokuapp.com/promos/${params.id}`)
      .then((res) => setPromo(res.data));
  }, [testComments]);
  console.log(promo);
  return (
    <Box className={classes.root}>
      <img
        src={promo.img}
        alt={promo.titulo}
        className={classes.gamePhoto}
      ></img>
      <Box className={classes.promoInfo}>
        <Box className={classes.return} onClick={() => history.push("/home")}>
          Voltar
        </Box>
        <Box className={classes.titleBox}>
          <Typography
            component="h3"
            variant="h3"
            className={classes.promotionTitle}
          >
            {promo.titulo}
          </Typography>
          <Box className={classes.likeBox}>
            <i
              className={"fas fa-thumbs-up like-button " + classes.likeIcon}
            ></i>
            <Typography
              component="h3"
              variant="h3"
              className={classes.likeCounter}
            ></Typography>
          </Box>
        </Box>
        <Typography
          component="h3"
          variant="h3"
          className={classes.platformName}
        >
          by {promo.plataforma}
        </Typography>
        <Typography component="h3" variant="h3" className={classes.price}>
          R$ - {promo.preco}
        </Typography>
        <Typography component="h3" variant="h3" className={classes.expires}>
          Promoção válida até: {promo.ExpirateDate}
        </Typography>
        <Typography component="h3" variant="h3" className={classes.creator}>
          Promoção criada por: {promo.nickname ? promo.nickname : "Usuário"}
        </Typography>
        <a href={promo.link} target="_blank" rel="noreferrer">
          <Button>Obter Jogo !</Button>
        </a>
      </Box>
      <Box className={classes.commentsSection}>
        <form
          className={classes.inputComment}
          onSubmit={handleSubmit(handleForm)}
        >
          <TextField
            placeholder="Comentar ..."
            variant="outlined"
            id="Comment"
            className={classes.inputBox}
            inputRef={register}
            error={!!errors.comment}
            helperText={errors.comment?.message}
            name="comment"
          ></TextField>
          <button className="commentButton">Salvar</button>
        </form>
        {testComments.map((comment, index) => (
          <Box key={index} className={classes.commentBox}>
            <Typography className={classes.commentAuthor}>
              {comment.name}
            </Typography>
            <Typography className={classes.commentText}>
              {comment.comment}
            </Typography>
          </Box>
        ))}
      </Box>
    </Box>
  );
}
