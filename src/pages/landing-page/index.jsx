import Header from "../../components/header";
import Footer from "../../components/footer";
import "./style.css";
import { useHistory } from "react-router-dom";
import Button from "../../components/button";
import { FaEye, FaEdit, FaThumbsUp } from "react-icons/fa";
import PromoCard from "../../components/promotion-card";
import LandingIllustration from "../../assets/illustrations/landingPageIllustration.svg";
import { listCardData } from "../../data/listCardData";

const LandingPage = () => {
  const history = useHistory();
  return (
    <>
      <Header />
      <div className="firstFrame">
        <div className="texto">
          <h1>Encontre games grátis disponíveis agora</h1>
          <p>
            As melhores promoções das melhores platadormas. Com o Free2Share,
            você sempre estará atualizado nos jogos mais quentes que estão
            disponíveis gratuitamente. Descubra, compartilhe, jogue.
          </p>
          <Button onClick={() => history.push("/register")}>Cadastre-se</Button>
        </div>
        <img src={LandingIllustration} alt="first-illustration" />
      </div>

      <div className="secondFrame">
        <div className="segundo-titulo">
          <h1>Na Free2Share você poderá:</h1>
        </div>
        <div className="segundo-feature-um">
          <h2>
            Acompanhar todos os games gratuitos no momento{" "}
            <span className="icon">
              <FaEye />
            </span>
          </h2>

          <p>
            Na página home você poderá navegar por todo o nosso acervo de
            promoções, ficando sempre informado sobre os melhores games
            gratuitos do momento.
          </p>
        </div>
        <div className="segundo-feature-dois">
          <h2>
            <span className="icon">
              <FaEdit />
            </span>{" "}
            Cadastrar novas promoções que acabaram de surgir
          </h2>
          <p>
            Seja parte da comunidade e ajude os jogadores a saberem daquela
            promoção que estavam todos esperando.
          </p>
        </div>
        <div className="segundo-feature-tres">
          <h2>
            Curtir e comentar as suas promoções preferidas{" "}
            <span className="icon">
              <FaThumbsUp />
            </span>
          </h2>
          <p>
            Salve suas promoções favoritas para poder adquirir o jogo mais
            tarde. Troque experiências com o resto da comunidade sobre seu jogo
            favorito.
          </p>
        </div>
      </div>

      <div className="third-frame">
        <h1 className="third-title">Destaques da Semana</h1>
        <div className="cards">
          {listCardData.map((card, index) => {
            return <PromoCard key={index} card={card} />;
          })}
        </div>
      </div>

      <Footer />
    </>
  );
};

export default LandingPage;
