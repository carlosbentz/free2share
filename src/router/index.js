import { Route, Switch, useHistory } from "react-router-dom";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "js-cookie";
import { setTokenThunk } from "../store/modules/token/thunk";
import { setUserThunk } from "../store/modules/user/thunk";

import LandingPage from "../pages/landing-page";
import LoginPage from "../pages/login-page";
import RegisterPage from "../pages/register-page";
import RegisterPromo from "../pages/registerPromo";
import ProfilePage from "../pages/profile-page";
import Navbar from "../components/navbar";
import PromotionPage from "../pages/promotion-page";
import Favoritos from "../pages/favoritos";
import Home from "../pages/home";

export default function Router() {
  const dispatch = useDispatch();
  const history = useHistory();
  const token = useSelector((store) => store.token);

  useEffect(() => {
    if (Cookies.get("token")) {
      dispatch(setTokenThunk(Cookies.get("token")));
      dispatch(setUserThunk(Cookies.get("token")));
    }
  }, []);

  return (
    <>
      <Switch>
        {token ? (
          <>
            <Route exact path="/home">
              <Home></Home>
            </Route>
            <Route path="/favorites">
              <Favoritos />
            </Route>
            <Route path="/add-promotion">
              <RegisterPromo />
            </Route>
            <Route path="/profile">
              <ProfilePage />
            </Route>
            <Route exact path="/login">
              {history.push("/home")}
            </Route>
            <Route exact path="/register">
              {history.push("/home")}
            </Route>
            <Route exact path="/">
              {history.push("/home")}
            </Route>
            <Route exact path="/promos/:id">
              <PromotionPage></PromotionPage>
            </Route>
          </>
        ) : (
          <>
            <Route path="/favorites">{history.push("/")}</Route>
            <Route path="/new-promotion">{history.push("/")}</Route>
            <Route path="/profile">{history.push("/")}</Route>
            <Route exact path="/">
              <LandingPage />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/register">
              <RegisterPage />
            </Route>
            <Route exact path="/promos/:id">
              <PromotionPage></PromotionPage>
            </Route>
          </>
        )}
      </Switch>
    </>
  );
}
